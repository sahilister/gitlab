Source: gitlab
Section: contrib/net
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>,
 Pirate Praveen <praveen@debian.org>,
 Balasankar C <balasankarc@autistici.org>,
 Sruthi Chandran <srud@debian.org>,
 Utkarsh Gupta <utkarsh@debian.org>
Build-Depends: debhelper (>= 10~),
               gem2deb,
               dh-golang,
               golang-any,
               bc,
               golang-github-alecthomas-chroma-dev,
               golang-github-aws-aws-sdk-go-dev,
               golang-github-azure-azure-storage-blob-go-dev (>= 0.10~),
               golang-github-beorn7-perks-dev,
               golang-github-client9-reopen-dev,
               golang-github-davecgh-go-spew-dev,
               golang-github-dgrijalva-jwt-go-dev (>= 3.2.0~),
               golang-github-disintegration-imaging-dev,
               golang-github-fzambia-sentinel-dev,
               golang-github-garyburd-redigo-dev,
               golang-github-google-uuid-dev,
               golang-github-getsentry-raven-go-dev (>= 0.2~),
               golang-github-gomodule-redigo-dev,
               golang-github-grpc-ecosystem-go-grpc-prometheus-dev,
               golang-github-jfbus-httprs-dev,
                 golang-github-jtolds-gls-dev (>= 4.20~),
               golang-github-jpillora-backoff-dev,
               golang-github-mitchellh-copystructure-dev,
               golang-github-mitchellh-reflectwalk-dev,
               golang-github-pmezard-go-difflib-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-prometheus-client-model-dev,
               golang-github-prometheus-common-dev,
               golang-github-ryszard-goskiplist-dev,
               golang-github-sebest-xff-dev,
               golang-github-sirupsen-logrus-dev,
               golang-github-stretchr-testify-dev (>= 1.4~),
               golang-gitlab-gitlab-org-labkit-dev (>= 1.3.0-4~),
               golang-gocloud-dev,
               golang-golang-x-crypto-dev,
               golang-golang-x-net-dev,
               golang-golang-x-sys-dev (>= 0.0~git20180510.7dfd129~),
               golang-google-genproto-dev,
               golang-goprotobuf-dev (>= 1.4.3~),
               golang-honnef-go-tools-dev,
               golang-procfs-dev,
               golang-protobuf-extensions-dev,
               golang-toml-dev,
               golang-websocket-dev,
               golang-google-grpc-dev (>= 1.22~),
               libimage-exiftool-perl
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/ruby-team/gitlab.git
Vcs-Browser: https://salsa.debian.org/ruby-team/gitlab
Homepage: https://about.gitlab.com/
XS-Ruby-Versions: all
XS-Go-Import-Path: gitlab.com/gitlab-org/gitlab-workhorse

Package: gitlab
Section: contrib/net
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${shlibs:Depends}, ${misc:Depends},
 gitlab-common (>= 13.7~),
 ruby2.7 (>= 2.7.2~),
   rubygems-integration (>= 1.18~),
 lsb-base (>= 3.0-6),
 rake (>= 12.3.0~),
 bundler (>= 1.17.3~),
 postgresql-client,
 postgresql-contrib,
 dbconfig-pgsql | dbconfig-no-thanks,
 bc,
 redis-server (>= 5:6.0.12~),
# See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956211
 nodejs (>= 10~),
 nginx | httpd,
 default-mta | postfix | exim4 | mail-transport-agent,
 openssh-client,
 bzip2,
 ucf,
 gitlab-workhorse (=${source:Version}),
 ruby-rails (>= 2:6.0.3.6~),
   ruby-tzinfo (>= 1.2.6~),
   ruby-websocket-extensions (>= 0.1.5~),
 ruby-bootsnap (>= 1.4.6~),
 ruby-responders (>= 3.0~),
 ruby-sprockets (>= 3.7~),
 ruby-default-value-for (>= 3.4~),
#ruby-mysql2 | ruby-pg,
 ruby-pg (>= 1.1~),
 ruby-rugged (>= 1.0~),
 ruby-grape-path-helpers (>= 1.6.1~),
 ruby-faraday (>= 1.0~),
 ruby-marginalia (>= 1.10~),
# Authentication libraries
 ruby-devise (>= 4.7.1~),
 ruby-bcrypt (>= 3.1.14~),
 ruby-doorkeeper (>= 5.5~),
 ruby-doorkeeper-openid-connect (>= 1.7.5~),
 ruby-rexml (>= 3.2.3.1~),
 ruby-saml (>= 1.12.1~),
 ruby-omniauth (>= 1.8~),
 ruby-omniauth-auth0 (>= 2.0~),
 ruby-omniauth-azure-activedirectory-v2,
 ruby-omniauth-azure-oauth2 (>= 0.0.10~),
 ruby-omniauth-cas3 (>= 1.1.4~),
 ruby-omniauth-facebook (>= 4.0~),
 ruby-omniauth-github (>= 1.4~),
 ruby-omniauth-gitlab (>= 1.0.2~),
 ruby-omniauth-google-oauth2 (>= 0.6~),
 ruby-omniauth-kerberos (>= 0.3.0-3~),
 ruby-omniauth-oauth2-generic (>= 0.2.2~),
 ruby-omniauth-saml (>= 1.10~),
 ruby-omniauth-shibboleth (>= 1.3~),
 ruby-omniauth-twitter (>= 1.4~),
 ruby-omniauth-crowd (>= 2.2~),
 ruby-omniauth-authentiq (>= 0.3.3~),
 ruby-omniauth-openid-connect (>= 0.3.5~),
 ruby-omniauth-salesforce (>= 1.0.5~),
 ruby-omniauth-atlassian-oauth2 (>= 0.2.0~),
 ruby-rack-oauth2 (>= 1.16~),
 ruby-jwt (>= 2.1~),
# Spam and anti-bot protection
 ruby-recaptcha (>= 4.11~),
 ruby-akismet (>= 3.0~),
 ruby-invisible-captcha (>= 1.1~),
# Two-factor authentication
 ruby-devise-two-factor (>= 4.0~),
 ruby-rqrcode-rails3 (>= 0.1.7~),
 ruby-attr-encrypted (>= 3.1.0-3~),
 ruby-u2f (>= 0.2.1~),
# GitLab Pages
 ruby-validates-hostname (>= 1.0.11~),
 ruby-zip (>= 2.0~),
# GitLab Pages letsencrypt support
 ruby-acme-client (>= 2.0.6~),
# Browser detection
 ruby-browser (>= 4.2~),
# OS detection for usage ping
 ohai (>= 16.10~),
# GPG
 ruby-gpgme (>= 2.0.19~),
# LDAP Auth
 ruby-omniauth-ldap (>= 2.1.1~),
   ruby-ntlm (>= 0.6.1~),
 ruby-net-ldap (>= 0.16.3~),
# API
 ruby-grape (>= 1.5.1~),
 ruby-grape-entity (>= 0.8~),
 ruby-rack-cors (>= 1.0.6~),
# GraphQL API
 ruby-graphql (>= 1.11.8~),
 ruby-graphiql-rails (>= 1.4.10~),
 ruby-apollo-upload-server (>= 2.0.3~),
# Used by BulkImport feature (group::import)
 ruby-graphlient (>= 0.4.0),
# Disable strong_params so that Mash does not respond to :permitted?
 ruby-hashie-forbidden-attributes,
# Pagination
 ruby-kaminari (>= 1.2.1~),
# HAML
 ruby-hamlit (>= 2.15~),
# Files attachments
 ruby-carrierwave (>= 1.3.2~),
 ruby-mini-magick (>= 4.10.1~),
# for backups
 ruby-fog-aws (>= 3.9~),
 ruby-fog-core (>= 2.1~),
   ruby-excon (>= 0.72~),
 ruby-fog-google (>= 1.13~),
 ruby-fog-local (>= 0.6~),
 ruby-fog-openstack (>= 1.0~),
 ruby-fog-rackspace (>= 0.1.1~),
 ruby-fog-aliyun (>= 0.3~),
 ruby-gitlab-fog-azure-rm (>= 1.0.1~),
   ruby-azure-storage-blob (>= 2.0.0-3~),
   ruby-azure-storage-common (>= 2.0.1-5~),
# for Google storage
 ruby-google-api-client (>= 0.44.2~),
# for aws storage
 ruby-unf (>= 0.1.4-2~),
   ruby-unf-ext (>= 0.0.7.4),
# Seed data
 ruby-seed-fu (>= 2.3.7~),
# Search
# ruby-elasticsearch-model (>= 0.1.9~), embedded
 ruby-elasticsearch (>= 5.0.3~),
# ruby-elasticsearch-rails (>= 0.1.9~), embedded
 ruby-elasticsearch-api (>= 6.8.2~),
 ruby-aws-sdk-core (>= 3.0~),
 ruby-aws-sdk-cloudformation (>= 1.0~),
 ruby-aws-sdk-s3 (>= 1.0~),
 ruby-faraday-middleware-aws-sigv4,
# Markdown and HTML processing
 ruby-html-pipeline (>= 2.13.2~),
 ruby-task-list (>= 2.3.1~),
 ruby-github-markup (>= 1.7~),
 ruby-commonmarker (>= 0.21~),
 ruby-kramdown (>= 2.3.1~),
 ruby-redcloth (>= 4.3.2-3~),
# rdoc is built-in with ruby
 ruby-org (>= 0.9.12-2~),
 ruby-creole (>= 0.5.0~),
 ruby-wikicloth (>= 0.8.1~),
 asciidoctor (>= 2.0.10~),
 ruby-asciidoctor-include-ext (>= 0.3.1~),
 ruby-asciidoctor-plantuml (>= 0.0.12~),
 ruby-asciidoctor-kroki (>= 0.4~),
 ruby-rouge (>= 3.21~),
 ruby-truncato (>= 0.7.11~),
 ruby-bootstrap-form (>= 4.2~),
 ruby-nokogiri (>= 1.11.1~),
 ruby-escape-utils (>= 1.2.1~),
# Calendar rendering
 ruby-icalendar,
# Diffs
 ruby-diffy (>= 3.3~),
 ruby-diff-match-patch (>= 0.1~),
# Application server
 ruby-rack (>= 2.2~),
 ruby-rack-timeout (>= 0.5.1~),
 puma (>= 5.1.1~),
 ruby-puma-worker-killer (>= 0.3.1~),
# State machine
 ruby-state-machines-activerecord (>= 0.8~),
   ruby-state-machines-activemodel (>= 0.7.1~),
# Issue tags
 ruby-acts-as-taggable-on (>= 7.0~),
# Background jobs
 ruby-sidekiq (>= 5.2.7~),
 ruby-sidekiq-cron (>= 1.0~),
 ruby-redis-namespace (>= 1.7~),
 ruby-gitlab-sidekiq-fetcher (>= 0.6.1~),
# Cron Parser
 ruby-fugit (>= 1.2.1~),
# HTTP requests
 ruby-httparty (>= 0.16.4~),
# Colored output to console
 ruby-rainbow (>= 3.0~),
# Progress bar
 ruby-progressbar (>= 1.10~),
# GitLab settings
 ruby-settingslogic (>= 2.0.9~),
# Linear-time regex library for untrusted regular expressions
 ruby-re2 (>= 1.2~),
# Misc
 ruby-version-sorter (>= 2.2.4~),
# Export Ruby Regex to Javascript
 ruby-js-regex (>= 3.4~),
# User agent parsing
 ruby-device-detector,
# Redis
 ruby-redis (>= 4.0~),
 ruby-connection-pool (>= 2.0~),
# Redis session store
 ruby-redis-rails (>= 5.0.2~),
   ruby-redis-activesupport (>= 5.2~),
   ruby-redis-actionpack (>= 5.2~),
# Discord integration
 ruby-discordrb-webhooks (>= 3.4~),
# JIRA integration
 ruby-jira (>= 2.1.4~),
# Flowdock integration
 ruby-flowdock (>= 0.7~),
   ruby-posix-spawn (>= 0.3.13~),
# Slack integration
 ruby-slack-messenger (>= 2.3.3~),
# Hangouts Chat integration
 ruby-hangouts-chat (>= 0.0.5),
# Asana integration
 ruby-asana (>= 0.10.3~),
# FogBugz integration
 ruby-fogbugz (>= 0.2.1-3~),
# Kubernetes integration
 ruby-kubeclient (>= 4.6~),
   ruby-recursive-open-struct (>= 1.1.1~),
   ruby-http (>= 4.4~),
# Sanitize user input
 ruby-sanitize (>= 5.2.1~),
 ruby-babosa (>= 1.0.3~),
# Sanitizes SVG input
 ruby-loofah (>= 2.2~),
# Working with license
 ruby-licensee (>= 9.14.1~),
# Protect against bruteforcing
 ruby-rack-attack (>= 6.3~),
# Ace editor
 ruby-ace-rails-ap (>= 4.1~),
# Detect and convert string character encoding
 ruby-charlock-holmes (>= 0.7.5~),
# Detect mime content type from content
 ruby-ruby-magic (>= 0.4~),
# Faster blank
 ruby-fast-blank,
# Parse time & duration
 ruby-gitlab-chronic (>= 0.10.5~),
 ruby-gitlab-chronic-duration (>= 0.10.6.2~),
#
 ruby-webpack-rails (>= 0.9.10~),
# Many node modules are still in NEW, some are yet to be packaged
# so we use yarn to downlod those and hence gitlab is in contrib
 yarnpkg (>= 1.22.4~),
 ruby-rack-proxy (>= 0.6~),
#
 ruby-sassc-rails (>= 2.1~),
   ruby-sassc (>= 2.0~),
 ruby-autoprefixer-rails (>= 10.2~),
 ruby-terser (>= 1.0.2~),
#
 ruby-addressable (>= 2.7~),
 ruby-gemojione (>= 3.3~),
 ruby-gon (>= 6.4~),
 ruby-request-store (>= 1.5~),
 ruby-jquery-atwho-rails (>= 1.3.2~),
 ruby-virtus (>= 1.0.5-3~),
 ruby-base32 (>= 0.3.0~),
# Sentry integration
 ruby-sentry-raven (>= 2.13~),
# PostgreSQL query parsing
 ruby-pg-query (>= 1.3~),
#
 ruby-premailer-rails (>= 1.10.3-2~),
# LabKit: Tracing and Correlation
 ruby-gitlab-labkit (>= 0.16.1~),
# Thrift is a dependency of gitlab-labkit, we want a version higher than 0.14.0
# because of https://gitlab.com/gitlab-org/gitlab/-/issues/321900
 ruby-thrift (>= 0.14~),
# I18n
 ruby-ruby-parser (>= 3.15~),
 ruby-rails-i18n (>= 6.0~),
 ruby-gettext-i18n-rails (>= 1.8~),
 ruby-gettext-i18n-rails-js (>= 1.3~),
   ruby-gettext (>= 3.3.3~),
#
 ruby-batch-loader (>= 2.0.1~),
# Perf bar
 ruby-peek (>= 1.1~),
# Snowplow events tracking
 ruby-snowplow-tracker (>= 0.6.1~),
# Memory benchmarks
 ruby-derailed-benchmarks,
# Metrics
 ruby-method-source (>= 1.0~),
# Prometheus
 ruby-prometheus-client-mmap (>= 0.12~),
 ruby-raindrops (>= 0.18~),
#
 ruby-octokit (>= 4.15~),
#
 ruby-mail-room (>= 0.10.0+really0.0.9~),
#
 ruby-email-reply-trimmer (>= 0.1~),
 ruby-html2text,
#
 ruby-prof (>= 1.3~),
 ruby-stackprof (>= 0.2.15~),
 ruby-rbtrace (>= 0.4~),
 ruby-memory-profiler (>= 0.9~),
 ruby-benchmark-memory (>= 0.1~),
 ruby-activerecord-explain-analyze (>= 0.1~),
# OAuth
 ruby-oauth2 (>= 1.4.4~),
# Health check
 ruby-health-check (>= 3.0~),
# System information
 ruby-vmstat (>= 2.3~),
 ruby-sys-filesystem (>= 1.1.6~),
# NTP client
 ruby-net-ntp,
# SSH host key support
 ruby-net-ssh (>= 1:6.0~),
 ruby-sshkey (>= 2.0~),
# Required for ED25519 SSH host key support
 ruby-ed25519 (>= 1.2~),
 ruby-bcrypt-pbkdf (>= 1.0~),
# Gitaly GRPC client
 ruby-gitaly (>= 13.11~),
# See #966653
# ruby-grpc (>= 1.30.2~),
 ruby-google-protobuf (>= 3.14~),
#
 ruby-toml-rb (>= 1.0.0-2~),
# Feature toggles
 ruby-flipper (>= 0.17.1~),
 ruby-flipper-active-record (>= 0.17.1~),
 ruby-flipper-active-support-cache-store (>= 0.17.1~),
 ruby-unleash (>= 0.1.5~),
 ruby-gitlab-experiment (>= 0.5~),
# Structured logging
 ruby-lograge (>= 0.10~),
 ruby-grape-logging (>= 1.7~),
# DNS Lookup
 ruby-gitlab-net-dns (>= 0.9.1~),
# Countries list
 ruby-countries (>= 3.0~),
 ruby-retriable (>= 3.1.2~),
# LRU cache
 ruby-lru-redux,
 ruby-erubi (>= 1.9~),
 ruby-mail (>= 2.7.1),
# File encryption
 ruby-lockbox (>= 0.6.2~),
# Email validation
 ruby-valid-email,
# JSON
 ruby-json (>= 2.3~),
 ruby-json-schema (>= 2.8.1-2~),
 ruby-json-schemer (>= 0.2.12~),
 ruby-oj (>= 3.10.6~),
 ruby-multi-json (>= 1.14.1~),
 ruby-yajl (>= 1.4.1~),
 ruby-webauthn (>= 2.3~),
 ruby-parslet,
# packaged node modules - all node packages are not packaged yet
 node-autosize (>= 4.0.2~dfsg1-5~),
 node-axios (>= 0.17.1~),
 node-babel7,
 node-babel-loader (>= 8.0~),
 node-babel-plugin-lodash,
 node-bootstrap,
 node-brace-expansion (>= 1.1.8~),
 node-cache-loader (>= 4.1~),
 node-chart.js (>= 2.7.2~),
 node-clipboard,
 node-codemirror,
 node-compression-webpack-plugin (>= 3.0.1~),
 node-copy-webpack-plugin (>= 5.0~),
 node-core-js (>= 3.2.1~),
 node-css-loader (>= 5.0~),
# node-d3 includes d3-sankey
 node-d3 (>= 5.16~),
 node-d3-scale (>= 1.0.7~),
 node-d3-selection (>= 1.2~),
 node-dateformat,
 node-deckar01-task-list (>= 2.2.1),
 node-exports-loader (>= 0.7~),
 node-imports-loader (>= 0.8~),
 node-file-loader (>= 5.0~),
 node-font-awesome,
 node-fuzzaldrin-plus (>= 0.5~),
 node-glob (>= 7.1.6~),
 node-jed (>= 1.1.1-2~),
 node-jquery (>= 3.5~),
 node-jquery-ujs,
# Broken
# node-jquery.waitforimages,
 node-js-cookie,
 node-js-yaml (>= 3.13.1~),
 node-jszip,
 node-jszip-utils (>= 0.0.2+dfsg-2~),
 node-katex (>= 0.10.2+dfsg-8~),
 node-lodash (>= 4.17.21+dfsg+~cs8.31.189.20210220~),
 node-marked (>= 0.3~),
 node-mermaid (>= 8.9.2~),
 node-minimatch,
 node-miragejs,
 node-mousetrap,
 node-pdfjs-dist,
# Include node-pikaday only after @gitlab/ui is accepted
# node-pikaday (>= 1.8.0-2~),
 node-popper.js (>= 1.16.1~),
 node-prismjs (>= 1.6~),
 node-prosemirror-model (>= 1.13.3~),
 node-prosemirror-markdown (>= 1.5.1~),
 node-raven-js,
 node-raw-loader (>= 4.0~),
 node-style-loader (>= 1.0~),
 node-three-orbit-controls (>= 82.1.0-3~),
 node-three-stl-loader (>= 1.0.4~),
 node-timeago.js (>= 4.0~),
 node-underscore (>= 1.9~),
 node-url-loader (>= 3.0~),
 node-uuid (>= 8.1~),
 node-vue (>= 2.6.10~),
 node-vue-resource (>= 1.5.1~),
# Blocked by #927254
# node-vue-router,
 webpack (>= 4.43~),
 node-webpack-stats-plugin,
 node-worker-loader (>= 2.0~),
 node-xterm,
# using yarn install for remaining node modules as it is in contrib
# node-babel-core,
# node-babel-eslint,
# node-babel-loader,
# node-babel-plugin-transform-define,
# node-babel-preset-latest,
# node-babel-preset-stage-2,
# node-debug (>= 3.1.0~),
# node-katex,
# node-marked,
# gitlab-sidekiq was failing without puma
 puma
Recommends: certbot,
 gitaly (>= 13.11~)
Conflicts: libruby2.5
Description: git powered software platform to collaborate on code (non-omnibus)
 gitlab provides web based interface to host source code and track issues.
 It allows anyone for fork a repository and send merge requests. Code review
 is possible using merge request workflow. Using groups and roles project
 access can be controlled.
 .
 Unlike the official package from GitLab Inc., this package does not use
 omnibus.
 .
 Note: Currently this package is in contrib because it uses yarn to install
 some of its front end dependencies.

Package: gitlab-workhorse
Section: net
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: unloads Git HTTP traffic from the GitLab Rails app (Unicorn)
 gitlab-workhorse was designed to unload Git HTTP traffic from the GitLab Rails
 app (Unicorn) to a separate daemon. It also serves 'git archive' downloads for
 GitLab. All authentication and authorization logic is still handled by the
 GitLab Rails app.
 .
 Architecture: Git client -> NGINX -> gitlab-workhorse (makes auth request to
 GitLab Rails app) -> git-upload-pack
